import React from 'react';
import './App.css';

const Table = (props) => {

    return (
        <table className="table mt-3">
            <thead>
                <tr>
                <th>Number</th>
                    <th>Message</th>
                    <th>Date</th>
                </tr>
            </thead>
               <tbody>
                    {props.data.map((value, index) => {
                        const mydate = new Date(value.date);
                        const newDate = mydate.getDate()+"/"+mydate.getMonth()+1 + "/"+mydate.getFullYear()+ "  "+ mydate.getHours()+":"+mydate.getMinutes();
                            return (
                                <tr key={index}>
                                        <th>{index+1}</th>
                                        <th>{value.msg.replace(/{|}|"/gi,"")}</th>
                                        <th>{newDate}</th>
                                </tr>
                            )
                        })}
             </tbody>
        </table>
    )

}

export default Table;
