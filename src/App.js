import React, {useState} from 'react';
import './App.css';
import axios from './axios/axios'
import Table from './Table';

const App = () => {
    const [inputValue,
        setInputValue] = useState('')
    const [data,
        setData] = useState('')

    const sendMessage = () => {
        if(inputValue !== ''){
            setInputValue('')
            axios
                .post('/', {"message": [inputValue]})
                .then(response => {
                    axios
                        .get('/')
                        .then(response => {
                            setData(response.data)
                        })
                        .catch(error => {
                            alert(error)
                        });

                })
                .catch(error => {
                    alert(error)
                });
            }
        else{alert('Please Enter Message')}
    }

    return (
        <div className="App">
            <input
                className="border-radius-5 margin-left-45 mt-2"
                value={inputValue}
                placeholder="Enter Message"
                type="text"
                onKeyDown={(e)=>{return e.key === 'Enter'?sendMessage():null}}
                onChange={(e) => setInputValue(e.target.value)}>
            </input>
            <button className=" border-radius-5 btn-primary " onClick={sendMessage}>Send</button>
            {data.length > 0
                ? <Table data={data}/>
                : null}

        </div>
    );
}

export default App;
