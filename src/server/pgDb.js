const {Client} = require('pg')
const client = new Client({user: 'postgres', host: 'localhost', database: 'testdb', password: '12345', port: 5433})
client.connect()

exports.fetchData = function () {
    return new Promise((resolve, reject) => {
        client.query('SELECT * from items.messages', (err, res) => {
            resolve(res.rows);
        });
    })
}

exports.upDateData = function (message) {
    client.query('INSERT INTO items.messages("msg","date") VALUES ($1,$2)', [message,[new Date()]], (err, res) => {
        console.log(err
            ? err.stack
            : res)
    })
}