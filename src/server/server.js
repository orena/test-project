var db = require('./pgDb')
var express = require('express');
var app = express();
var bodyParser = require('body-parser')
app.use(bodyParser.json())
var cors = require('cors')
app.use(cors())
app.get('/', function (req, res) {
    db.fetchData()
        .then(resolve => {
            res.send(resolve);
        })
});

app.post('/', function (req, res) {
    db.upDateData(req.body.message)
    res.send("Success")
})
app.listen(8081);